package main

import (
	"bufio"
	// "errors"
	"fmt"
	// "math"
	"os"
	// "sort"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	strs := strings.Split(lines[0], ",")
	nums := make([]int, len(strs))

	for i, str := range strs {
		num, err := strconv.Atoi(str)
		if err != nil {
			panic(err)
		}
		nums[i] = num
	}

	recents := make(map[int]int)
	second_recents := make(map[int]int)
	just_said := 0
	// for i := 0; i < 2020; i++ {
	for i := 0; i < 30000000; i++ {
		if i < len(nums) {
			just_said = nums[i]
			recents[nums[i]] = i
		} else {
			value, present := recents[just_said]
			_, present2 := second_recents[just_said]
			if !present2 {
				just_said = 0
			} else {
				just_said = value - second_recents[just_said]
			}
			_, present = recents[just_said]
			if present {
				second_recents[just_said] = recents[just_said]
			}
			recents[just_said] = i
		}
	}
	fmt.Println(just_said)
}
