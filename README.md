My advent of code solutions.

Currently, I only have 2020, written in Go (I'm learning the language).

These are only part 2 solutions, although many of them have commented-out part 1
solutions visible as well.

Each directory is a day; in it, there is one Go file containing the part 2 (and
commented part 1?) solution. My problem input is "input"; the example input may
be "input2" if it exists (it's not always necessary to test with it).
