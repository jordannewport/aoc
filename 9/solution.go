package main

import (
	"bufio"
	// "errors"
	"fmt"
	"os"
	// "sort"
	"strconv"
	// "strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]int, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		val, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		lines = append(lines, val)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	var target int

	for i, val := range lines {
		if i < 25 {
			continue
		}

		valid := false
		for j := i-25; j < i; j++ {
			for k := j; k < i; k++ {
				if lines[j] + lines[k] == val {
					valid = true
				}
			}
		}
		if !valid {
			target = val
			break
		}
	}
	fmt.Println(target)

	first_index := 0
	last_index := 0
	sum := lines[0]
	for i, val := range lines {
		if i == 0 {
			continue
		}
		sum += val
		for sum > target {
			sum -= lines[first_index]
			first_index++
		}
		if sum == target {
			last_index = i
			largest := val
			smallest := val
			for j := first_index; j <= last_index; j++ {
				if lines[j] > largest {
					largest = lines[j]
				}
				if lines[j] < smallest {
					smallest = lines[j]
				}
			}
			fmt.Println(largest + smallest)
			break
		}
	}
	// 1016 too low
}
