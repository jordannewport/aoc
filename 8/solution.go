package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	// "sort"
	"strconv"
	"strings"
)

func simulate(lines []string) (int, error) {
	pc := 0
	acc := 0
	visited := make(map[int]bool, 0)

	for {
		if pc >= len(lines) {
			// this is if we managed to hit the end
			return acc, nil
		}
		_, present := visited[pc]
		if present {
			break
		} else {
			visited[pc] = true
		}
		split := strings.Split(lines[pc], " ")
		num, err := strconv.Atoi(split[1][1:])
		if err != nil {
			panic(err)
		}
		sign := split[1][0]
		if sign == '-' {
			num = -num
		}
		switch split[0] {
		case "acc":
			acc += num
		case "jmp":
			pc += num
			continue
		case "nop":
			if pc + num == len(lines) {
				fmt.Println(pc)
				break
			}
		}
		pc++
	}
	return acc, errors.New("did not hit end")
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	for i, line := range lines {
		split := strings.Split(line, " ")
		if split[0] == "jmp" {
			new_line := strings.Replace(line, "jmp", "nop", 1)
			new_lines := make([]string, len(lines))
			copy(new_lines, lines)
			new_lines[i] = new_line
			acc, err := simulate(new_lines)
			if err == nil {
				fmt.Println(acc)
				break
			}
		}
	}

	fmt.Println(simulate(lines))
}
