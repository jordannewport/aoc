package main

import (
	"bufio"
	"fmt"
	"os"
	// "sort"
	"strconv"
	"strings"
)

/*
func bag_contains(cont map[string][]bagset, start, target string) bool {
	for _, v := range cont[start] {
		if v == target {
			return true
		}
		if bag_contains(cont, v, target) {
			return true
		}
	}
	return false
}
*/

func num_contains(cont map[string][]bagset, name string) int {
	sum := 0
	for _, v := range cont[name] {
		sum += v.amount
		sum += v.amount * num_contains(cont, v.name)
	}
	return sum
}

type bagset struct {
	amount int
	name string
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	cont := make(map[string][]bagset)

	total := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, " ")
		outer_color := split[0] + " " + split[1]
		if strings.Contains(line, "no other bags") {
			cont[outer_color] = make([]bagset, 0)
			continue
		}
		inner_colors := make([]bagset, 0)
		was_number := false
		first := ""
		for i, word := range split {
			if len(first) > 0 {
				inner_color := first + " " + word
				first = ""
				number, err := strconv.Atoi(split[i-2])
				if err != nil {
					panic(err)
				}
				inner_colors = append(inner_colors, bagset{number, inner_color})
			}
			if was_number {
				first = word
				was_number = false
			}
			if strings.Contains("0123456789", word) {
				was_number = true
			}
		}
		cont[outer_color] = inner_colors
	}

	fmt.Println(num_contains(cont, "shiny gold"))

	/*
	for bag := range cont {
		if bag_contains(cont, bag, "shiny gold") {
			total++
		}
	}
	*/

	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	fmt.Println(total)
}
