package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	nums := make([]int, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		value, err := strconv.Atoi(scanner.Text())
		if err != nil {
			panic(err)
		}
		nums = append(nums, value)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	fmt.Println(nums)

	for _, num := range nums {
		for _, num2 := range nums {
			for _, num3 := range nums {
				if (num + num2 + num3) == 2020 {
					fmt.Println(num * num2 * num3)
				}
			}
		}
	}
}
