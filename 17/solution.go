package main

import (
	"bufio"
	// "errors"
	"fmt"
	// "math"
	"os"
	// "sort"
	// "strconv"
	"strings"
)

func simulate(dimension [][]string) [][]string {
	x := len(dimension[0][0])
	y := len(dimension[0])
	z := len(dimension)

	// surround in 2 sets of empty cubes
	new_dim := make([][]string, 0)

	var plane []string
	// bottom rows
	for h := 0; h < 2; h++ {
		plane = make([]string, 0)
		for i := 0; i < y + 4; i++ {
			row := ""
			for j := 0; j < x + 4; j++ {
				row += "."
			}
			plane = append(plane, row)
		}
		new_dim = append(new_dim, plane)
	}

	for i := 0; i < z; i++ {
		plane = make([]string, 0)
		for j := 0; j < y + 4; j++ {
			row := ""
			if j <= 1 || j >= y + 2 {
				for k := 0; k < x + 4; k++ {
					row += "."
				}
			} else {
				row = ".." + dimension[i][j-2] + ".."
			}
			plane = append(plane, row)
		}
		new_dim = append(new_dim, plane)
	}

	// top rows
	for h := 0; h < 2; h++ {
		plane = make([]string, 0)
		for i := 0; i < y + 4; i++ {
			row := ""
			for j := 0; j < x + 4; j++ {
				row += "."
			}
			plane = append(plane, row)
		}
		new_dim = append(new_dim, plane)
	}

	// simulate
	result := make([][]string, 0)
	for i, plane := range new_dim {
		result_plane := make([]string, 0)
		if i == 0 || i == len(new_dim) - 1 {
			result_plane = plane
			continue
		}
		for j, row := range plane {
			result_string := ""
			if j == 0 || j == len(plane) - 1 {
				result_string = row
				continue
			}
			for k, cell := range row {
				if k == 0 || k == len(row) - 1 {
					result_string += "."
					continue
				}
				neighbors := ""
				neighbors += new_dim[i-1][j-1][k-1:k+2]
				neighbors += new_dim[i-1][j][k-1:k+2]
				neighbors += new_dim[i-1][j+1][k-1:k+2]

				neighbors += new_dim[i][j-1][k-1:k+2]
				neighbors += string(new_dim[i][j][k-1]) + string(new_dim[i][j][k+1])
				neighbors += new_dim[i][j+1][k-1:k+2]

				neighbors += new_dim[i+1][j-1][k-1:k+2]
				neighbors += new_dim[i+1][j][k-1:k+2]
				neighbors += new_dim[i+1][j+1][k-1:k+2]

				occupied := strings.Count(neighbors, "#")
				if cell == '#' && (occupied == 2 || occupied == 3) {
					result_string += "#"
				} else if cell == '.' && occupied == 3 {
					result_string += "#"
				} else {
					result_string += "."
				}
			}
			result_plane = append(result_plane, result_string)
		}
		result = append(result, result_plane)
	}
	return result
}

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	dimension := make([][]string, 1)
	dimension[0] = lines

	for i := 0; i < 6; i++ {
		dimension = simulate(dimension)
	}

	sum := 0
	for _, plane := range dimension {
		for _, row := range plane {
			sum += strings.Count(row, "#")
		}
	}
	fmt.Println(sum)
}
