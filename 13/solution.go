package main

import (
	"bufio"
	// "errors"
	"fmt"
	// "math"
	"os"
	// "sort"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	/*
	target_time, err := strconv.Atoi(lines[0])
	if err != nil {
		panic(err)
	}
	*/

	times := strings.Split(lines[1], ",")

	/*
	new_times := make([]int, 0)
	for _, time := range times {
		if time != "x" {
			val, err := strconv.Atoi(time)
			if err != nil {
				panic(err)
			}
			new_times = append(new_times, val)
		}
	}

	min_wait := math.Inf(0)
	bus := 0
	for _, time := range new_times {
		var wait float64 = float64(time - (target_time % time))
		if wait < min_wait {
			min_wait = wait
			bus = time
		}
	}
	fmt.Println(int(min_wait) * bus)
	*/

	new_times := make([][2]int, 0)
	for i, time := range times {
		if time != "x" {
			val, err := strconv.Atoi(time)
			if err != nil {
				panic(err)
			}
			line := [2]int{val, i}
			new_times = append(new_times, line) // time, window offset
		}
	}

	candidate_time := 0
	candidate_window := 1
	fmt.Println(candidate_time, candidate_window)
	for i, time := range new_times {
		fmt.Println()
		fmt.Println("i", i)
		fmt.Println("time", time)
		delta := candidate_window
		window := candidate_window * time[0]
		fmt.Println("delta", delta)
		fmt.Println("window", window)
		for j := candidate_time + time[1]; j < window; j += delta {
			if j % time[0] == 0 {
				candidate_time = j - time[1]
				break
			}
		}
		candidate_window = window
		fmt.Println("candidate", candidate_time, candidate_window)
	}
	// should be 0, 77/78, 350/351/354 for the example
	fmt.Println()
	fmt.Println(candidate_time, candidate_window)
}
