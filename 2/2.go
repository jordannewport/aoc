package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	var count_invalid int = 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		split := strings.Split(line, " ")
		bounds := strings.Split(split[0], "-")
		lower_bound, err := strconv.Atoi(bounds[0])
		if err != nil {
			panic(err)
		}
		upper_bound, err := strconv.Atoi(bounds[1])
		if err != nil {
			panic(err)
		}
		letter := split[1][0]
		password := split[2]
		fmt.Println(lower_bound, upper_bound, string(letter), password)
		fmt.Println(string(password[lower_bound-1]), string(password[upper_bound-1]))
		if (password[lower_bound-1] == letter) == (password[upper_bound-1] == letter) {
			count_invalid++
			fmt.Println("invalid")
		} else {
			fmt.Println("valid")
		}
		/*
		for _, c := range password {
			if c == letter {
				fmt.Println(c)
				count++
			}
		}
		if count > upper_bound || count < lower_bound {
			count_invalid++
		}
		*/
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}
	fmt.Println(1000 - count_invalid)
}
