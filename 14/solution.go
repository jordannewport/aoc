package main

import (
	"bufio"
	// "errors"
	"fmt"
	// "math"
	"os"
	// "sort"
	"strconv"
	"strings"
)

func floating(bits string) map[string]bool {
	if !strings.Contains(bits, "X") {
		result := make(map[string]bool)
		result[bits] = true
		return result
	}
	result := make(map[string]bool)
	b0 := strings.Replace(bits, "X", "0", 1)
	r0 := floating(b0)
	for k, _ := range r0 {
		result[k] = true
	}
	b1 := strings.Replace(bits, "X", "1", 1)
	r1 := floating(b1)
	for k, _ := range r1 {
		result[k] = true
	}
	return result
}

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	/*
	mask := [36]int{}
	memory := make(map[string]int64)
	for _, line := range lines {
		split := strings.Split(line, " ")
		if split[0] == "mask" {
			for j := 35; j >= 0; j-- {
				switch split[2][36-j-1] {
				case '0':
					mask[j] = 0
				case '1':
					mask[j] = 1
				case 'X':
					mask[j] = -1
				}
			}
		} else {
			value, err := strconv.Atoi(split[2])
			if err != nil {
				panic(err)
			}
			bits := strconv.FormatInt(int64(value), 2)
			new_string := ""
			lb := len(bits)
			for j, mv := range mask {
				switch mv {
				case 0:
					new_string = "0" + new_string
				case 1:
					new_string = "1" + new_string
				case -1:
					if j >= lb {
						new_string = "0" + new_string
					} else {
						new_string = string(bits[lb-j-1]) + new_string
					}
				}
			}
			val_to_write, err := strconv.ParseInt(new_string, 2, 64)
			if err != nil {
				panic(err)
			}
			memory[split[0]] = val_to_write
		}
	}
	var sum int64 = 0
	for _, val := range memory {
		sum += val
	}
	*/
	mask := [36]int{}
	memory := make(map[string]int64)
	for _, line := range lines {
		split := strings.Split(line, " ")
		if split[0] == "mask" {
			for j := 35; j >= 0; j-- {
				switch split[2][36-j-1] {
				case '0':
					mask[j] = 0
				case '1':
					mask[j] = 1
				case 'X':
					mask[j] = -1
				}
			}
		} else {
			value, err := strconv.Atoi(split[2])
			if err != nil {
				panic(err)
			}
			mem_addr := split[0][4:len(split[0])-1]
			addr_val, err := strconv.Atoi(mem_addr)
			if err != nil {
				panic(err)
			}
			bits := strconv.FormatInt(int64(addr_val), 2)
			new_string := ""
			for j, mv := range mask {
				switch mv {
				case 0:
					if j < len(bits) {
						new_string = string(bits[len(bits)-j-1]) + new_string
					} else {
						new_string = "0" + new_string
					}
				case 1:
					new_string = "1" + new_string
				case -1:
					new_string = "X" + new_string
				}
			}
			floats := floating(new_string)
			for k, _ := range floats {
				memory[k] = int64(value)
			}
		}
	}
	var sum int64 = 0
	for _, val := range memory {
		sum += val
	}
	fmt.Println(sum)
}
