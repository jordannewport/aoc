package main

import (
	"bufio"
	// "errors"
	"fmt"
	"os"
	// "sort"
	// "strconv"
	"strings"
)

func scan(deck []string, x, y int) string {
	result := ""
	directions := [8][2]int{
		{-1, -1},
		{-1, 0},
		{-1, 1},
		{0, -1},
		{0, 1},
		{1, -1},
		{1, 0},
		{1, 1},
	}
	for _, dir := range directions {
		new_x := x
		new_y := y
		for {
			new_x += dir[0]
			new_y += dir[1]
			if new_y < 0 || new_x < 0 || new_y > (len(deck) - 1) || new_x > (len(deck[new_y]) - 1) {
				result += "."
				break
			}
			if deck[new_y][new_x] == 'L' || deck[new_y][new_x] == '#' {
				result += string(deck[new_y][new_x])
				break
			}
		}
	}
	return result
}

func simulate(deck []string) []string {
	// surround in empty seats
	new_deck := make([]string, 0)
	top_and_bottom_line := ""
	for i := 0; i < len(deck[0]) + 2; i++ {
		top_and_bottom_line += "."
	}
	new_deck = append(new_deck, top_and_bottom_line)
	for _, line := range deck {
		new_deck = append(new_deck, "." + line + ".")
	}
	new_deck = append(new_deck, top_and_bottom_line)

	result := make([]string, 0)
	// simulate
	for i, line := range new_deck {
		result_line := ""
		for j, spot := range line {
			result_char := spot
			if spot == 'L' {
				// neighbors := new_deck[i-1][j-1:j+2] + string(line[j-1]) + string(line[j+1]) + new_deck[i+1][j-1:j+2]
				neighbors := scan(new_deck, j, i)
				occupied := strings.Count(neighbors, "#")
				if occupied == 0 {
					result_char = '#'
				}
			} else if spot == '#' {
				// neighbors := new_deck[i-1][j-1:j+2] + string(line[j-1]) + string(line[j+1]) + new_deck[i+1][j-1:j+2]
				neighbors := scan(new_deck, j, i)
				occupied := strings.Count(neighbors, "#")
				if occupied >= 5 {
					result_char = 'L'
				}
			}
			if j > 0 && j < (len(line) - 1) {
				result_line += string(result_char)
			}
		}
		if i > 0 && i < (len(new_deck) - 1) {
			result = append(result, result_line)
		}
	}
	return result
}

func same(left, right []string) bool {
	if len(left) != len(right) {
		return false
	}
	for i, v := range left {
		if v != right[i] {
			return false
		}
	}
	return true
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}
	i := 0
	for !same(lines, simulate(lines)) {
		lines = simulate(lines)
		i++
		/*
		for _, line := range lines {
			fmt.Println(line)
		}
		fmt.Println()
		*/
	}
	spots := ""
	for _, line := range lines {
		spots += line
	}
	fmt.Println(strings.Count(spots, "#"))
}
