package main

import (
	"bufio"
	"fmt"
	"os"
	"sort"
	"strconv"
	"strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// var highest_seatid int64 = 0
	// highest_seat := ""

	seatids := make([]int64, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		row := line[0:7]
		col := line[7:]
		row = strings.Replace(row, "F", "0", -1)
		row = strings.Replace(row, "B", "1", -1)
		rownum, err := strconv.ParseInt(row, 2, 64)
		if err != nil {
			panic(err)
		}
		col = strings.Replace(col, "L", "0", -1)
		col = strings.Replace(col, "R", "1", -1)
		colnum, err := strconv.ParseInt(col, 2, 64)
		if err != nil {
			panic(err)
		}
		seatid := rownum * 8 + colnum
		/*
		if seatid > highest_seatid {
			highest_seatid = seatid
			highest_seat = line
		}
		*/
		seatids = append(seatids, seatid)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	sort.Slice(seatids, func(i, j int) bool {return seatids[i] < seatids[j]})

	for i, seatid := range seatids {
		if i == 0 {
			continue
		}
		if seatid != seatids[i-1] + 1 {
			fmt.Println(seatids[i-1] + 1)
		}
	}

	// fmt.Println(highest_seatid)
	// fmt.Println(highest_seat)
}
