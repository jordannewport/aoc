package main

import (
	"bufio"
	// "errors"
	"fmt"
	// "math"
	"os"
	// "sort"
	"strconv"
	"strings"
)

type field struct {
	name string
	start, end int
	start2, end2 int
}

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	fields := make([]field, 0)
	var last_field int

	// make fields
	for i, line := range lines {
		if len(line) == 0 {
			last_field = i
			break
		}
		name_split := strings.Split(line, ":")
		split := strings.Split(name_split[1], " ")
		range1 := strings.Split(split[1], "-")
		range2 := strings.Split(split[3], "-")
		start, err := strconv.Atoi(range1[0])
		if err != nil {
			panic(err)
		}
		end, err := strconv.Atoi(range1[1])
		if err != nil {
			panic(err)
		}
		start2, err := strconv.Atoi(range2[0])
		if err != nil {
			panic(err)
		}
		end2, err := strconv.Atoi(range2[1])
		if err != nil {
			panic(err)
		}

		my_field := field{name_split[0], start, end, start2, end2}
		fields = append(fields, my_field)
	}

	/*
	found_first_ticket := false
	invalid_values := make([]int, 0)
	for i, ticket := range lines {
		if i <= last_field {
			continue
		}
		if len(ticket) == 0 {
			found_first_ticket = true
			continue
		}
		if !found_first_ticket || ticket == "nearby tickets:" {
			continue
		}
		ticket_fields := strings.Split(ticket, ",")
		ticket_is_valid := true
		for _, ticket_field := range ticket_fields {
			value, err := strconv.Atoi(ticket_field)
			if err != nil {
				panic(err)
			}
			valid := false
			for _, field := range fields {
				if !((value < field.start || value > field.end) && (value < field.start2 || value > field.end2)) {
					valid = true
					break
				}
			}
			if !valid {
				invalid_values = append(invalid_values, value)
			}
		}
	}
	sum := 0
	for _, value := range invalid_values {
		sum += value
	}
	fmt.Println(sum)
	*/

	found_first_ticket := false
	var my_ticket string
	valid_tickets := make([]string, 0)
	for i, ticket := range lines {
		if i <= last_field {
			continue
		}
		if i == last_field + 2 {
			my_ticket = ticket
		}
		if len(ticket) == 0 {
			found_first_ticket = true
			continue
		}
		if !found_first_ticket || ticket == "nearby tickets:" {
			continue
		}
		ticket_fields := strings.Split(ticket, ",")
		ticket_is_valid := true
		for _, ticket_field := range ticket_fields {
			value, err := strconv.Atoi(ticket_field)
			if err != nil {
				panic(err)
			}
			valid := false
			for _, field := range fields {
				if !((value < field.start || value > field.end) && (value < field.start2 || value > field.end2)) {
					valid = true
					break
				}
			}
			if !valid {
				ticket_is_valid = false
				break
			}
		}
		if ticket_is_valid {
			valid_tickets = append(valid_tickets, ticket)
		}
	}

	my_ticket_fields := make([]int, 0)
	tf := strings.Split(my_ticket, ",")
	for _, f := range tf {
		value, err := strconv.Atoi(f)
		if err != nil {
			panic(err)
		}
		my_ticket_fields = append(my_ticket_fields, value)
	}
	fmt.Println(my_ticket_fields)

	valid_ticket_fields := make([][]int, len(valid_tickets))
	for i, ticket := range valid_tickets {
		ticket_fields := strings.Split(ticket, ",")
		list := make([]int, len(ticket_fields))
		for j, ticket_field := range ticket_fields {
			value, err := strconv.Atoi(ticket_field)
			if err != nil {
				panic(err)
			}
			list[j] = value
		}
		valid_ticket_fields[i] = list
	}
	fmt.Println(fields)

	valids := make(map[string]map[int]bool)
	// try all fields for each position
	for _, field := range fields {
		valids[field.name] = make(map[int]bool)
		for i := 0; i < len(valid_ticket_fields[0]); i++ {
			valid := true
			for _, ticket := range valid_ticket_fields {
				if (ticket[i] < field.start || ticket[i] > field.end) && (ticket[i] < field.start2 || ticket[i] > field.end2) {
					valid = false
					break
				}
			}
			if valid {
				valids[field.name][i] = true
			}
		}
	}
	fmt.Println(valids)
	all_1 := false
	for !all_1 {
		all_1 = true
		for k, v := range valids {
			if len(v) == 1 {
				var value int
				for k2 := range v {
					value = k2
				}
				for k2, v2 := range valids {
					if k2 != k {
						delete(v2, value)
					}
				}
			} else {
				all_1 = false
			}
		}
	}
	fmt.Println(valids)
	product := 1
	for k, v := range valids {
		if len(k) >= 9 && k[0:9] == "departure" {
			for k2, _ := range v {
				product *= my_ticket_fields[k2]
			}
		}
	}
	fmt.Println(product)
}
