package main

import (
	"bufio"
	// "errors"
	"fmt"
	"os"
	"sort"
	"strconv"
	// "strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	joltages := make([]int, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		val, err := strconv.Atoi(line)
		if err != nil {
			panic(err)
		}
		joltages = append(joltages, val)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	sort.Ints(joltages)

	diff1 := 0
	diff2 := 0
	diff3 := 1 // for final laptop charger
	diffs := make([]int, 0)
	old := 0
	for _, joltage := range joltages {
		switch joltage - old {
		case 1:
			diff1++
		case 2:
			diff2++
		case 3:
			diff3++
		}
		diffs = append(diffs, joltage - old)
		old = joltage
	}
	diffs = append(diffs, 3) // for final charger
	fmt.Println(diffs)
	// fmt.Println(diff1)
	// fmt.Println(diff3)
	// fmt.Println(diff1 * diff3)
	run := 0
	runs := make([]int, 0)
	for _, diff := range diffs {
		if diff == 1 {
			run++
		} else {
			// can't skip in small runs
			if run > 1 {
				runs = append(runs, run)
			}
			run = 0
		}
	}
	options := 1
	// I checked and the biggest run is 4
	// these are Pascal's triangle #s
	for _, run := range runs {
		switch run {
		case 2:
			options *= 2
		case 3:
			options *= 4
		case 4:
			// you can't skip all 3
			options *= 7
		}
	}
	fmt.Println(runs)
	fmt.Println(options)
}
