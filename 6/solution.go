package main

import (
	"bufio"
	"fmt"
	"os"
	// "sort"
	// "strconv"
	// "strings"
)

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	group := make(map[rune]int)

	total := 0

	n := 0
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			for _, int := range group {
				if int == n {
					total++
				}
			}
			group = make(map[rune]int)
			n = 0
		} else {
			for _, char := range line {
				_, present := group[char]
				if present {
					group[char]++
				} else {
					group[char] = 1
				}
			}
			n++
		}
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

	fmt.Println(total)
}
