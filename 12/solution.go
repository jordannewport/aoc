package main

import (
	"bufio"
	// "errors"
	"fmt"
	"math"
	"os"
	// "sort"
	"strconv"
	// "strings"
)

func main() {
	file, err := os.Open("input")
	// file, err := os.Open("input2")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	lines := make([]string, 0)

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		lines = append(lines, line)
	}
	if err := scanner.Err(); err != nil {
		panic(err)
	}

	x := 10
	y := 1
	// direction := 0

	ship_x := 0
	ship_y := 0

	for _, line := range lines {
		distance, err := strconv.Atoi(line[1:])
		if err != nil {
			panic(err)
		}
		switch line[0] {
		case 'N':
			y += distance
		case 'E':
			x += distance
		case 'S':
			y -= distance
		case 'W':
			x -= distance
			/*
				case 'L':
					direction += distance
				case 'R':
					direction -= distance
				case 'F':
					for direction < 0 {
						direction += 360
					}
					switch direction % 360 {
					case 0:
						x += distance
					case 90:
						y += distance
					case 180:
						x -= distance
					case 270:
						y -= distance
					}
			*/
		case 'L':
			for i := 0; i < (distance / 90); i++ {
				new_x := -y
				y = x
				x = new_x
			}
		case 'R':
			for i := 0; i < (distance / 90); i++ {
				new_x := y
				y = -x
				x = new_x
			}
		case 'F':
			ship_x += (x * distance)
			ship_y += (y * distance)
		}
	}

	fmt.Println(ship_x, ship_y)
	fmt.Println(math.Abs(float64(ship_x)) + math.Abs(float64(ship_y)))
	// 1497 too high
}
