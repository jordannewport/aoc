package main

import (
	"bufio"
	"fmt"
	"os"
	"strconv"
	"strings"
)

func make_record () map[string]string {
	record := make(map[string]string)
	record["byr"] = ""
	record["iyr"] = ""
	record["eyr"] = ""
	record["hgt"] = ""
	record["hcl"] = ""
	record["ecl"] = ""
	record["pid"] = ""
	record["cid"] = ""
	return record
}

func contains (val string, slice []string) bool {
	for _, element := range slice {
		if element == val {
			return true
		}
	}
	return false
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

	record := make_record()

	fail := 0
	n := 0

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
		if len(line) == 0 {
			n++
			old_record := record
			record = make_record()

			byr, err := strconv.Atoi(old_record["byr"])
			if err != nil {
				fail++
				continue
			}
			if byr < 1920 || byr > 2002 {
				fail++
				continue
			}
			iyr, err := strconv.Atoi(old_record["iyr"])
			if err != nil {
				fail++
				continue
			}
			if iyr < 2010 || iyr > 2020 {
				fail++
				continue
			}
			eyr, err := strconv.Atoi(old_record["eyr"])
			if err != nil {
				fail++
				continue
			}
			if eyr < 2020 || eyr > 2030 {
				fail++
				continue
			}
			if len(old_record["hgt"]) < 2 {
				fail++
				continue
			}
			if old_record["hgt"][len(old_record["hgt"])-2:len(old_record["hgt"])] == "cm" {
				hgt, err := strconv.Atoi(old_record["hgt"][0:3])
				if err != nil {
					fail++
					continue
				}
				if hgt < 150 || hgt > 193 {
					fail++
					continue
				}
			} else if old_record["hgt"][len(old_record["hgt"])-2:len(old_record["hgt"])] == "in" {
				hgt, err := strconv.Atoi(old_record["hgt"][0:2])
				if err != nil {
					fail++
					continue
				}
				if hgt < 59 || hgt > 76 {
					fail++
					continue
				}
			} else {
				fail++
				continue
			}
			if len(old_record["hcl"]) != 7 || old_record["hcl"][0] != '#' {
				fail++
				continue
			}
			flag := true
			for i, char := range old_record["hcl"] {
				if i == 0 {
					continue
				}
				if !contains(string(char), []string{"0","1","2","3","4","5","6","7","8","9","a","b","c","d","e","f"}) {
					fail++
					flag = false
					break
				}
			}
			if !flag {
				continue
			}
			if !contains(old_record["ecl"], []string{"amb","blu","brn","gry","grn","hzl","oth"}) {
				fail++
				continue
			}
			if len(old_record["pid"]) != 9 {
				fail++
				continue
			}
			for _, char := range old_record["pid"] {
				if !contains(string(char), []string{"0","1","2","3","4","5","6","7","8","9"}) {
					fail++
					flag = false
					break
				}
			}
			if !flag {
				continue
			}

		} else {
			var fields []string = strings.Split(line, " ")
			for _, field := range fields {
				kv := strings.Split(field, ":")
				record[kv[0]] = kv[1]
			}
		}
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}
	fmt.Println(n - fail)
	fmt.Println(n)
}
