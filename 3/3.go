package main

import (
	"bufio"
	"fmt"
	"os"
	// "strconv"
	// "strings"
)

func count_collisions(right, down int, tile []string) int {
    height := len(tile)
    current_x := -right
    collisions := 0
    for current_y := 0; current_y < height; current_y+= down {
        current_x += right
        if current_x >= len(tile[current_y]) {
            current_x %= len(tile[current_y])
        }
        if tile[current_y][current_x] == '#' {
            collisions++
        }
    }
    return collisions
}

func main() {
	file, err := os.Open("input")
	if err != nil {
		panic(err)
	}
	defer file.Close()

    tile := make([]string, 0)
	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		line := scanner.Text()
        tile = append(tile, line)
	}
	if err :=scanner.Err(); err != nil {
		panic(err)
	}

    c := count_collisions(1, 1, tile)
    c *= count_collisions(3, 1, tile)
    c *= count_collisions(5, 1, tile)
    c *= count_collisions(7, 1, tile)
    c *= count_collisions(1, 2, tile)
    fmt.Println(c)
}
